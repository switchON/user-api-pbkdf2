# coding: utf-8
import sys

# from flask import Flask, send_file, send_from_directory
# from user_api.blueprint import construct_user_api_blueprint
# from config import CONFIG
# import MySQLdb
# #
# app = Flask(__name__)
# 
# app.register_blueprint(
#     construct_user_api_blueprint(
#         db_driver=MySQLdb,
#         db_host=u"35.185.107.179",
#         db_user=u"appengine",
#         db_passwd=u"meYyTGaBjQVk8s4E",
#         db_name=u"user_api",
#         jwt_secret=CONFIG[u"auth"][u"token"][u"secret"],
#         jwt_lifetime=CONFIG[u"auth"][u"token"][u"lifetime"]
#     ),
#     url_prefix=u"/api/user"
# )
# 
# app.run(port=5000, debug=True)


from user_api.authentication import Authentication
from user_api.db_manager import DBManager


db_manager=DBManager(
    db_driver=MySQLdb,
    db_host=u"35.185.107.179",
    db_user=u"appengine",
    db_passwd=u"meYyTGaBjQVk8s4E",
    db_name=u"user_api"
)

auth = Authentication(
    jwt_secret=CONFIG[u"auth"][u"token"][u"secret"],
    jwt_lifetime=CONFIG[u"auth"][u"token"][u"lifetime"]
)

test = 0
# Create user

if test == 0:
    hash, salt = auth.generate_hash_and_salt(u"test911")
    db_manager.save_new_user(u"switch9111@gmail.com", u"Admin", hash, salt)
    print(salt)
    print(hash)

elif test == 1:
    payload = db_manager.get_user_informations(u"switch9111@gmail.com")
    token = auth.generate_token(payload=payload)
    print(token)
    data = auth.get_token_data(token=token)
    print(data)
    valid = auth.is_token_valid(token)
    print(valid)