# -*- coding: utf-8 -*-
from user_api.db_manager import DBManager
from user_api.authentication import Authentication

from flask import request, jsonify, Blueprint
import json


def construct_user_api_blueprint(
        db_driver,
        db_host,
        db_user,
        db_passwd,
        db_name,
        jwt_secret,
        jwt_lifetime=3600
):
    """
    Take the various parameters necessary to the blue print to work, then
    construct the blueprint from them, to inject in flask configuration.
    :param db_driver:
    :param db_host:
    :param db_user:
    :param db_passwd:
    :param db_name:
    :param jwt_secret:
    :param jwt_lifetime:
    :return: Return a blueprint.
    """

    # Init DB Manager
    db_manager = DBManager(
        db_driver,
        db_host=db_host,
        db_user=db_user,
        db_passwd=db_passwd,
        db_name=db_name
    )
    # Init Auth Manager
    auth = Authentication(
        jwt_secret=jwt_secret,
        jwt_lifetime=jwt_lifetime
    )
    user_api_blueprint = Blueprint(u'user_api', __name__)

    @user_api_blueprint.route(u'/authentify', methods=[u"POST"])
    def user():
        try:
            data = json.loads(request.data, encoding=u"utf-8")
        except ValueError:
            return jsonify({
                u"message": u"Invalid JSON."
            }), 422
        if u"password" in data and u"email" in data:
            try:
                email = data.get(u"email")
                password = data.get(u"password")
                salt = db_manager.get_user_salt(email=email)
                hash = auth.generate_hash(
                    password,
                    salt
                )
                valid = db_manager.is_user_hash_valid(
                    email,
                    hash=hash
                )

                if valid:
                    payload = db_manager.get_user_informations(email=email)
                    token = auth.generate_token(payload)
                else:
                    raise ValueError(u"Wrong password")

                return jsonify({
                    u"token": token
                }), 200
            except ValueError:
                return jsonify({
                    u"message": u"Wrong login or / and password."
                }), 401
        else:
            return jsonify({
                u"message": u"Missing parameters."
            }), 422

    @user_api_blueprint.route(u'/reset_password', methods=[u'POST'])
    def reset_password():
        try:
            data = json.loads(request.data, encoding=u"utf-8")
        except ValueError:
            return jsonify({
                u"message": u"Invalid JSON."
            }), 422
        if u"password" in data and u"email" in data:
            try:
                salt = auth.generate_salt()
                email = data.get(u"email")
                password = data.get(u"password")
                hash = auth.generate_hash(password, salt)

                db_manager.modify_hash_salt(email, hash, salt)
                payload = db_manager.get_user_informations(email=email)
                token = auth.generate_token(payload)

                return jsonify({
                    u"token": token
                }), 200
            except ValueError:
                return jsonify({
                    u"message": u"Wrong login or / and password."
                }), 401
        else:
            return jsonify({
                u"message": u"Missing parameters."
            }), 422

    @user_api_blueprint.route(u'/register', methods=[u"POST"])
    def register():
        try:
            data = json.loads(request.data, encoding=u"utf-8")
        except ValueError:
            return jsonify({
                u"message": u"Invalid JSON."
            }), 422
        if u"password" in data and u"email" in data and u"name" in data:
            try:
                salt = auth.generate_salt()
                hash = auth.generate_hash(data.get(u"password"), salt)
                db_manager.save_new_user(
                    email=data.get(u"email"),
                    name=data.get(u"name"),
                    hash=hash,
                    salt=salt
                )
            except ValueError:
                return jsonify({
                    u"message": u"User already exists."
                }), 409
            return jsonify({
                u"message": u"New user registered successfully."
            }), 201
        else:
            return jsonify({
                u"message": u"Missing parameters."
            }),

    @user_api_blueprint.route(u'/token/check/', methods=[u"POST"])
    def check():
        try:
            data = json.loads(request.data, encoding=u"utf-8")
        except ValueError:
            return jsonify({
                u"message": u"Invalid JSON."
            }), 422
        if u"token" in data:
            decoded = auth.get_token_data(data[u"token"])
            if decoded is not None:
                return jsonify(decoded), 200
            else:
                return jsonify({
                    u"message": u"Invalid token."
                }), 401
        else:
            return jsonify({
                u"message": u"Missing parameters."
            }), 422

    return user_api_blueprint
